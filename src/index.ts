#!/usr/bin/env node

import chalk from 'chalk';
import figlet from 'figlet';
import clear from 'clear';
import program from 'commander';

import { configureService } from './configure';

clear();
console.log(chalk.red(figlet.textSync('nicky-cli', { horizontalLayout: 'full' })));

program.version('0.0.1');

program
    .command('configure')
    .alias('c')
    .description('Configure a new translation service!')
    .action(async () => {
        const config = await configureService();
        console.log(config);
    });

program.parse(process.argv);
